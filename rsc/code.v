set_property -dict { PACKAGE_PIN L18 IOSTANDARD LVCMOS33 } [get_ports { V2BIND_p }];
set_property -dict { PACKAGE_PIN M13 IOSTANDARD LVCMOS33 } [get_ports { V2AIND_p }];
set_property -dict { PACKAGE_PIN M18 IOSTANDARD LVCMOS33 } [get_ports { V3BIND_p }];
set_property -dict { PACKAGE_PIN P15 IOSTANDARD LVCMOS33 } [get_ports { PMODC1_p }];
set_property -dict { PACKAGE_PIN R12 IOSTANDARD LVCMOS33 } [get_ports { V1AIND_p }];
set_property -dict { PACKAGE_PIN R13 IOSTANDARD LVCMOS33 } [get_ports { V1BIND_p }];
set_property -dict { PACKAGE_PIN R15 IOSTANDARD LVCMOS33 } [get_ports { PMODC5_p }];
set_property -dict { PACKAGE_PIN R16 IOSTANDARD LVCMOS33 } [get_ports { V3AIND_p }];
set_property -dict { PACKAGE_PIN T9 IOSTANDARD LVCMOS33 } [get_ports { PMODC7_p }];
set_property -dict { PACKAGE_PIN T10 IOSTANDARD LVCMOS33 } [get_ports { PMODC3_p }];
set_property -dict { PACKAGE_PIN T11 IOSTANDARD LVCMOS33 } [get_ports { PMODC2_p }];
set_property -dict { PACKAGE_PIN T14 IOSTANDARD LVCMOS33 } [get_ports { PMODC0_p }];
set_property -dict { PACKAGE_PIN T15 IOSTANDARD LVCMOS33 } [get_ports { PMODC4_p }];
set_property -dict { PACKAGE_PIN U11 IOSTANDARD LVCMOS33 } [get_ports { PMODC6_p }];
set_property -dict { PACKAGE_PIN K3 IOSTANDARD LVCMOS33 } [get_ports { W1ALS1_p }];
set_property -dict { PACKAGE_PIN K5 IOSTANDARD LVCMOS33 } [get_ports { FPGA_DIP2_p }];
set_property -dict { PACKAGE_PIN L1 IOSTANDARD LVCMOS33 } [get_ports { W1BLS2_p }];
set_property -dict { PACKAGE_PIN L3 IOSTANDARD LVCMOS33 } [get_ports { W1AHS1_p }];
set_property -dict { PACKAGE_PIN L4 IOSTANDARD LVCMOS33 } [get_ports { FPGA_DIP3_p }];
set_property -dict { PACKAGE_PIN L5 IOSTANDARD LVCMOS33 } [get_ports { PMODE6_p }];
set_property -dict { PACKAGE_PIN L6 IOSTANDARD LVCMOS33 } [get_ports { PMODE2_p }];
set_property -dict { PACKAGE_PIN M1 IOSTANDARD LVCMOS33 } [get_ports { W1BHS2_p }];
set_property -dict { PACKAGE_PIN M2 IOSTANDARD LVCMOS33 } [get_ports { W2BHS1_p }];
set_property -dict { PACKAGE_PIN M3 IOSTANDARD LVCMOS33 } [get_ports { W2BLS1_p }];
set_property -dict { PACKAGE_PIN M4 IOSTANDARD LVCMOS33 } [get_ports { AUXSPI_MOSI_p }];
set_property -dict { PACKAGE_PIN M6 IOSTANDARD LVCMOS33 } [get_ports { FPGA_DIP0_p }];
set_property -dict { PACKAGE_PIN N1 IOSTANDARD LVCMOS33 } [get_ports { W2ALS1_p }];
set_property -dict { PACKAGE_PIN N2 IOSTANDARD LVCMOS33 } [get_ports { W2AHS1_p }];
set_property -dict { PACKAGE_PIN N4 IOSTANDARD LVCMOS33 } [get_ports { AUXSPI_MISO_p }];
set_property -dict { PACKAGE_PIN N5 IOSTANDARD LVCMOS33 } [get_ports { W1ALS2_p }];
set_property -dict { PACKAGE_PIN N6 IOSTANDARD LVCMOS33 } [get_ports { FPGA_DIP1_p }];
set_property -dict { PACKAGE_PIN P2 IOSTANDARD LVCMOS33 } [get_ports { QSPI_CLK_p }];
set_property -dict { PACKAGE_PIN P3 IOSTANDARD LVCMOS33 } [get_ports { W2ALS2_p }];
set_property -dict { PACKAGE_PIN P4 IOSTANDARD LVCMOS33 } [get_ports { W2AHS2_p }];
set_property -dict { PACKAGE_PIN P5 IOSTANDARD LVCMOS33 } [get_ports { W1AHS2_p }];
set_property -dict { PACKAGE_PIN R1 IOSTANDARD LVCMOS33 } [get_ports { W2BHS2_p }];
set_property -dict { PACKAGE_PIN R2 IOSTANDARD LVCMOS33 } [get_ports { QSPI_IO3_p }];
set_property -dict { PACKAGE_PIN R3 IOSTANDARD LVCMOS33 } [get_ports { QSPI_IO0_p }];
set_property -dict { PACKAGE_PIN R5 IOSTANDARD LVCMOS33 } [get_ports { AUXSPI_NSS_p }];
set_property -dict { PACKAGE_PIN R6 IOSTANDARD LVCMOS33 } [get_ports { AUXSPI_CLK_p }];
set_property -dict { PACKAGE_PIN R7 IOSTANDARD LVCMOS33 } [get_ports { W3AIND_p }];
set_property -dict { PACKAGE_PIN R8 IOSTANDARD LVCMOS33 } [get_ports { PMODE7_p }];
set_property -dict { PACKAGE_PIN T1 IOSTANDARD LVCMOS33 } [get_ports { W2BLS2_p }];
set_property -dict { PACKAGE_PIN T3 IOSTANDARD LVCMOS33 } [get_ports { QSPI_NCS_p }];
set_property -dict { PACKAGE_PIN T4 IOSTANDARD LVCMOS33 } [get_ports { W3ALS1_p }];
set_property -dict { PACKAGE_PIN T5 IOSTANDARD LVCMOS33 } [get_ports { W3AHS1_p }];
set_property -dict { PACKAGE_PIN T6 IOSTANDARD LVCMOS33 } [get_ports { W3BIND_p }];
set_property -dict { PACKAGE_PIN T8 IOSTANDARD LVCMOS33 } [get_ports { PMODE3_p }];
set_property -dict { PACKAGE_PIN U1 IOSTANDARD LVCMOS33 } [get_ports { W3ALS2_p }];
set_property -dict { PACKAGE_PIN U2 IOSTANDARD LVCMOS33 } [get_ports { W1BLS1_p }];
set_property -dict { PACKAGE_PIN U3 IOSTANDARD LVCMOS33 } [get_ports { W3BHS1_p }];
set_property -dict { PACKAGE_PIN U4 IOSTANDARD LVCMOS33 } [get_ports { W3BLS1_p }];
set_property -dict { PACKAGE_PIN U6 IOSTANDARD LVCMOS33 } [get_ports { QSPI_IO2_p }];
set_property -dict { PACKAGE_PIN U7 IOSTANDARD LVCMOS33 } [get_ports { QSPI_IO1_p }];
set_property -dict { PACKAGE_PIN U9 IOSTANDARD LVCMOS33 } [get_ports { W2BIND_p }];
set_property -dict { PACKAGE_PIN V1 IOSTANDARD LVCMOS33 } [get_ports { W3AHS2_p }];
set_property -dict { PACKAGE_PIN V2 IOSTANDARD LVCMOS33 } [get_ports { W1BHS1_p }];
set_property -dict { PACKAGE_PIN V4 IOSTANDARD LVCMOS33 } [get_ports { W1BIND_p }];
set_property -dict { PACKAGE_PIN V5 IOSTANDARD LVCMOS33 } [get_ports { W1AIND_p }];
set_property -dict { PACKAGE_PIN V6 IOSTANDARD LVCMOS33 } [get_ports { W3BHS2_p }];
set_property -dict { PACKAGE_PIN V7 IOSTANDARD LVCMOS33 } [get_ports { W3BLS2_p }];
set_property -dict { PACKAGE_PIN V9 IOSTANDARD LVCMOS33 } [get_ports { W2AIND_p }];
set_property -dict { PACKAGE_PIN A11 IOSTANDARD LVCMOS33 } [get_ports { PMODE0_p }];
set_property -dict { PACKAGE_PIN A13 IOSTANDARD LVCMOS33 } [get_ports { PMODD3_p }];
set_property -dict { PACKAGE_PIN A14 IOSTANDARD LVCMOS33 } [get_ports { PMODD7_p }];
set_property -dict { PACKAGE_PIN A15 IOSTANDARD LVCMOS33 } [get_ports { PMODD0_p }];
set_property -dict { PACKAGE_PIN A16 IOSTANDARD LVCMOS33 } [get_ports { PMODD4_p }];
set_property -dict { PACKAGE_PIN A18 IOSTANDARD LVCMOS33 } [get_ports { BTN_DIRW_p }];
set_property -dict { PACKAGE_PIN B11 IOSTANDARD LVCMOS33 } [get_ports { PMODE4_p }];
set_property -dict { PACKAGE_PIN B12 IOSTANDARD LVCMOS33 } [get_ports { V1ALS2_p }];
set_property -dict { PACKAGE_PIN B13 IOSTANDARD LVCMOS33 } [get_ports { V1ALS1_p }];
set_property -dict { PACKAGE_PIN B14 IOSTANDARD LVCMOS33 } [get_ports { V1AHS1_p }];
set_property -dict { PACKAGE_PIN B16 IOSTANDARD LVCMOS33 } [get_ports { FPGA_LED2_p }];
set_property -dict { PACKAGE_PIN B17 IOSTANDARD LVCMOS33 } [get_ports { FPGA_LED1_p }];
set_property -dict { PACKAGE_PIN B18 IOSTANDARD LVCMOS33 } [get_ports { BTN_DIRS_p }];
set_property -dict { PACKAGE_PIN C12 IOSTANDARD LVCMOS33 } [get_ports { V1AHS2_p }];
set_property -dict { PACKAGE_PIN C15 IOSTANDARD LVCMOS33 } [get_ports { PMODE1_p }];
set_property -dict { PACKAGE_PIN C16 IOSTANDARD LVCMOS33 } [get_ports { V1BHS1_p }];
set_property -dict { PACKAGE_PIN C17 IOSTANDARD LVCMOS33 } [get_ports { V1BLS1_p }];
set_property -dict { PACKAGE_PIN D12 IOSTANDARD LVCMOS33 } [get_ports { FPGA_LED3_p }];
set_property -dict { PACKAGE_PIN D13 IOSTANDARD LVCMOS33 } [get_ports { FPGA_LED4_p }];
set_property -dict { PACKAGE_PIN D15 IOSTANDARD LVCMOS33 } [get_ports { PMODE5_p }];
set_property -dict { PACKAGE_PIN D17 IOSTANDARD LVCMOS33 } [get_ports { V1BHS2_p }];
set_property -dict { PACKAGE_PIN D18 IOSTANDARD LVCMOS33 } [get_ports { V3BHS2_p }];
set_property -dict { PACKAGE_PIN E15 IOSTANDARD LVCMOS33 } [get_ports { V2ALS1_p }];
set_property -dict { PACKAGE_PIN E16 IOSTANDARD LVCMOS33 } [get_ports { V2AHS1_p }];
set_property -dict { PACKAGE_PIN E17 IOSTANDARD LVCMOS33 } [get_ports { V1BLS2_p }];
set_property -dict { PACKAGE_PIN E18 IOSTANDARD LVCMOS33 } [get_ports { V3BLS2_p }];
set_property -dict { PACKAGE_PIN F13 IOSTANDARD LVCMOS33 } [get_ports { BTN_DIRN_p }];
set_property -dict { PACKAGE_PIN F14 IOSTANDARD LVCMOS33 } [get_ports { BTN_DIRE_p }];
set_property -dict { PACKAGE_PIN F15 IOSTANDARD LVCMOS33 } [get_ports { PMODD2_p }];
set_property -dict { PACKAGE_PIN F16 IOSTANDARD LVCMOS33 } [get_ports { PMODD6_p }];
set_property -dict { PACKAGE_PIN F18 IOSTANDARD LVCMOS33 } [get_ports { V3ALS1_p }];
set_property -dict { PACKAGE_PIN G14 IOSTANDARD LVCMOS33 } [get_ports { BTN_DIRC_p }];
set_property -dict { PACKAGE_PIN G16 IOSTANDARD LVCMOS33 } [get_ports { PMODD5_p }];
set_property -dict { PACKAGE_PIN G17 IOSTANDARD LVCMOS33 } [get_ports { V2ALS2_p }];
set_property -dict { PACKAGE_PIN G18 IOSTANDARD LVCMOS33 } [get_ports { V3AHS1_p }];
set_property -dict { PACKAGE_PIN H14 IOSTANDARD LVCMOS33 } [get_ports { BTN_DIRB_p }];
set_property -dict { PACKAGE_PIN H15 IOSTANDARD LVCMOS33 } [get_ports { V3ALS2_p }];
set_property -dict { PACKAGE_PIN H16 IOSTANDARD LVCMOS33 } [get_ports { PMODD1_p }];
set_property -dict { PACKAGE_PIN H17 IOSTANDARD LVCMOS33 } [get_ports { V2AHS2_p }];
set_property -dict { PACKAGE_PIN J13 IOSTANDARD LVCMOS33 } [get_ports { V2BHS2_p }];
set_property -dict { PACKAGE_PIN J14 IOSTANDARD LVCMOS33 } [get_ports { V3AHS2_p }];
set_property -dict { PACKAGE_PIN J15 IOSTANDARD LVCMOS33 } [get_ports { V2BHS1_p }];
set_property -dict { PACKAGE_PIN J17 IOSTANDARD LVCMOS33 } [get_ports { V3BHS1_p }];
set_property -dict { PACKAGE_PIN J18 IOSTANDARD LVCMOS33 } [get_ports { V3BLS1_p }];
set_property -dict { PACKAGE_PIN K13 IOSTANDARD LVCMOS33 } [get_ports { V2BLS2_p }];
set_property -dict { PACKAGE_PIN K15 IOSTANDARD LVCMOS33 } [get_ports { V2BLS1_p }];
set_property -dict { PACKAGE_PIN A1 IOSTANDARD LVCMOS33 } [get_ports { U3BIND_p }];
set_property -dict { PACKAGE_PIN A3 IOSTANDARD LVCMOS33 } [get_ports { U1AIND_p }];
set_property -dict { PACKAGE_PIN A4 IOSTANDARD LVCMOS33 } [get_ports { U1BIND_p }];
set_property -dict { PACKAGE_PIN A5 IOSTANDARD LVCMOS33 } [get_ports { U3BLS2_p }];
set_property -dict { PACKAGE_PIN A6 IOSTANDARD LVCMOS33 } [get_ports { U3BHS2_p }];
set_property -dict { PACKAGE_PIN B1 IOSTANDARD LVCMOS33 } [get_ports { U3AIND_p }];
set_property -dict { PACKAGE_PIN B2 IOSTANDARD LVCMOS33 } [get_ports { U2AIND_p }];
set_property -dict { PACKAGE_PIN B3 IOSTANDARD LVCMOS33 } [get_ports { U2BIND_p }];
set_property -dict { PACKAGE_PIN B4 IOSTANDARD LVCMOS33 } [get_ports { U2BLS2_p }];
set_property -dict { PACKAGE_PIN B6 IOSTANDARD LVCMOS33 } [get_ports { U3BLS1_p }];
set_property -dict { PACKAGE_PIN B7 IOSTANDARD LVCMOS33 } [get_ports { U3BHS1_p }];
set_property -dict { PACKAGE_PIN C4 IOSTANDARD LVCMOS33 } [get_ports { U2BHS2_p }];
set_property -dict { PACKAGE_PIN C5 IOSTANDARD LVCMOS33 } [get_ports { U3ALS2_p }];
set_property -dict { PACKAGE_PIN C6 IOSTANDARD LVCMOS33 } [get_ports { U3AHS2_p }];
set_property -dict { PACKAGE_PIN C7 IOSTANDARD LVCMOS33 } [get_ports { U3ALS1_p }];
set_property -dict { PACKAGE_PIN D2 IOSTANDARD LVCMOS33 } [get_ports { U1BLS1_p }];
set_property -dict { PACKAGE_PIN D7 IOSTANDARD LVCMOS33 } [get_ports { U2BLS1_p }];
set_property -dict { PACKAGE_PIN D8 IOSTANDARD LVCMOS33 } [get_ports { U3AHS1_p }];
set_property -dict { PACKAGE_PIN E2 IOSTANDARD LVCMOS33 } [get_ports { U1BHS1_p }];
set_property -dict { PACKAGE_PIN E7 IOSTANDARD LVCMOS33 } [get_ports { U2BHS1_p }];
set_property -dict { PACKAGE_PIN F6 IOSTANDARD LVCMOS33 } [get_ports { U2AHS1_p }];
set_property -dict { PACKAGE_PIN G2 IOSTANDARD LVCMOS33 } [get_ports { U2AHS2_p }];
set_property -dict { PACKAGE_PIN G3 IOSTANDARD LVCMOS33 } [get_ports { U1BLS2_p }];
set_property -dict { PACKAGE_PIN G4 IOSTANDARD LVCMOS33 } [get_ports { U1BHS2_p }];
set_property -dict { PACKAGE_PIN G6 IOSTANDARD LVCMOS33 } [get_ports { U2ALS1_p }];
set_property -dict { PACKAGE_PIN H2 IOSTANDARD LVCMOS33 } [get_ports { U2ALS2_p }];
set_property -dict { PACKAGE_PIN H4 IOSTANDARD LVCMOS33 } [get_ports { U1AHS1_p }];
set_property -dict { PACKAGE_PIN H5 IOSTANDARD LVCMOS33 } [get_ports { U1AHS2_p }];
set_property -dict { PACKAGE_PIN H6 IOSTANDARD LVCMOS33 } [get_ports { U1ALS2_p }];
set_property -dict { PACKAGE_PIN J4 IOSTANDARD LVCMOS33 } [get_ports { U1ALS1_p }];


module uemu_top(
	output wire V2BIND_p,
);

module uemu_top(
	output wire V2AIND_p,
);

module uemu_top(
	output wire V3BIND_p,
);

module uemu_top(
	output wire PMODC1_p,
);

module uemu_top(
	output wire V1AIND_p,
);

module uemu_top(
	output wire V1BIND_p,
);

module uemu_top(
	output wire PMODC5_p,
);

module uemu_top(
	output wire V3AIND_p,
);

module uemu_top(
	output wire PMODC7_p,
);

module uemu_top(
	output wire PMODC3_p,
);

module uemu_top(
	output wire PMODC2_p,
);

module uemu_top(
	output wire PMODC0_p,
);

module uemu_top(
	output wire PMODC4_p,
);

module uemu_top(
	output wire PMODC6_p,
);

module uemu_top(
	output wire W1ALS1_p,
);

module uemu_top(
	output wire FPGA_DIP2_p,
);

module uemu_top(
	output wire W1BLS2_p,
);

module uemu_top(
	output wire W1AHS1_p,
);

module uemu_top(
	output wire FPGA_DIP3_p,
);

module uemu_top(
	output wire PMODE6_p,
);

module uemu_top(
	output wire PMODE2_p,
);

module uemu_top(
	output wire W1BHS2_p,
);

module uemu_top(
	output wire W2BHS1_p,
);

module uemu_top(
	output wire W2BLS1_p,
);

module uemu_top(
	output wire AUXSPI_MOSI_p,
);

module uemu_top(
	output wire FPGA_DIP0_p,
);

module uemu_top(
	output wire W2ALS1_p,
);

module uemu_top(
	output wire W2AHS1_p,
);

module uemu_top(
	output wire AUXSPI_MISO_p,
);

module uemu_top(
	output wire W1ALS2_p,
);

module uemu_top(
	output wire FPGA_DIP1_p,
);

module uemu_top(
	output wire QSPI_CLK_p,
);

module uemu_top(
	output wire W2ALS2_p,
);

module uemu_top(
	output wire W2AHS2_p,
);

module uemu_top(
	output wire W1AHS2_p,
);

module uemu_top(
	output wire W2BHS2_p,
);

module uemu_top(
	output wire QSPI_IO3_p,
);

module uemu_top(
	output wire QSPI_IO0_p,
);

module uemu_top(
	output wire AUXSPI_NSS_p,
);

module uemu_top(
	output wire AUXSPI_CLK_p,
);

module uemu_top(
	output wire W3AIND_p,
);

module uemu_top(
	output wire PMODE7_p,
);

module uemu_top(
	output wire W2BLS2_p,
);

module uemu_top(
	output wire QSPI_NCS_p,
);

module uemu_top(
	output wire W3ALS1_p,
);

module uemu_top(
	output wire W3AHS1_p,
);

module uemu_top(
	output wire W3BIND_p,
);

module uemu_top(
	output wire PMODE3_p,
);

module uemu_top(
	output wire W3ALS2_p,
);

module uemu_top(
	output wire W1BLS1_p,
);

module uemu_top(
	output wire W3BHS1_p,
);

module uemu_top(
	output wire W3BLS1_p,
);

module uemu_top(
	output wire QSPI_IO2_p,
);

module uemu_top(
	output wire QSPI_IO1_p,
);

module uemu_top(
	output wire W2BIND_p,
);

module uemu_top(
	output wire W3AHS2_p,
);

module uemu_top(
	output wire W1BHS1_p,
);

module uemu_top(
	output wire W1BIND_p,
);

module uemu_top(
	output wire W1AIND_p,
);

module uemu_top(
	output wire W3BHS2_p,
);

module uemu_top(
	output wire W3BLS2_p,
);

module uemu_top(
	output wire W2AIND_p,
);

module uemu_top(
	output wire PMODE0_p,
);

module uemu_top(
	output wire PMODD3_p,
);

module uemu_top(
	output wire PMODD7_p,
);

module uemu_top(
	output wire PMODD0_p,
);

module uemu_top(
	output wire PMODD4_p,
);

module uemu_top(
	output wire BTN_DIRW_p,
);

module uemu_top(
	output wire PMODE4_p,
);

module uemu_top(
	output wire V1ALS2_p,
);

module uemu_top(
	output wire V1ALS1_p,
);

module uemu_top(
	output wire V1AHS1_p,
);

module uemu_top(
	output wire FPGA_LED2_p,
);

module uemu_top(
	output wire FPGA_LED1_p,
);

module uemu_top(
	output wire BTN_DIRS_p,
);

module uemu_top(
	output wire V1AHS2_p,
);

module uemu_top(
	output wire PMODE1_p,
);

module uemu_top(
	output wire V1BHS1_p,
);

module uemu_top(
	output wire V1BLS1_p,
);

module uemu_top(
	output wire FPGA_LED3_p,
);

module uemu_top(
	output wire FPGA_LED4_p,
);

module uemu_top(
	output wire PMODE5_p,
);

module uemu_top(
	output wire V1BHS2_p,
);

module uemu_top(
	output wire V3BHS2_p,
);

module uemu_top(
	output wire V2ALS1_p,
);

module uemu_top(
	output wire V2AHS1_p,
);

module uemu_top(
	output wire V1BLS2_p,
);

module uemu_top(
	output wire V3BLS2_p,
);

module uemu_top(
	output wire BTN_DIRN_p,
);

module uemu_top(
	output wire BTN_DIRE_p,
);

module uemu_top(
	output wire PMODD2_p,
);

module uemu_top(
	output wire PMODD6_p,
);

module uemu_top(
	output wire V3ALS1_p,
);

module uemu_top(
	output wire BTN_DIRC_p,
);

module uemu_top(
	output wire PMODD5_p,
);

module uemu_top(
	output wire V2ALS2_p,
);

module uemu_top(
	output wire V3AHS1_p,
);

module uemu_top(
	output wire BTN_DIRB_p,
);

module uemu_top(
	output wire V3ALS2_p,
);

module uemu_top(
	output wire PMODD1_p,
);

module uemu_top(
	output wire V2AHS2_p,
);

module uemu_top(
	output wire V2BHS2_p,
);

module uemu_top(
	output wire V3AHS2_p,
);

module uemu_top(
	output wire V2BHS1_p,
);

module uemu_top(
	output wire V3BHS1_p,
);

module uemu_top(
	output wire V3BLS1_p,
);

module uemu_top(
	output wire V2BLS2_p,
);

module uemu_top(
	output wire V2BLS1_p,
);

module uemu_top(
	output wire U3BIND_p,
);

module uemu_top(
	output wire U1AIND_p,
);

module uemu_top(
	output wire U1BIND_p,
);

module uemu_top(
	output wire U3BLS2_p,
);

module uemu_top(
	output wire U3BHS2_p,
);

module uemu_top(
	output wire U3AIND_p,
);

module uemu_top(
	output wire U2AIND_p,
);

module uemu_top(
	output wire U2BIND_p,
);

module uemu_top(
	output wire U2BLS2_p,
);

module uemu_top(
	output wire U3BLS1_p,
);

module uemu_top(
	output wire U3BHS1_p,
);

module uemu_top(
	output wire U2BHS2_p,
);

module uemu_top(
	output wire U3ALS2_p,
);

module uemu_top(
	output wire U3AHS2_p,
);

module uemu_top(
	output wire U3ALS1_p,
);

module uemu_top(
	output wire U1BLS1_p,
);

module uemu_top(
	output wire U2BLS1_p,
);

module uemu_top(
	output wire U3AHS1_p,
);

module uemu_top(
	output wire U1BHS1_p,
);

module uemu_top(
	output wire U2BHS1_p,
);

module uemu_top(
	output wire U2AHS1_p,
);

module uemu_top(
	output wire U2AHS2_p,
);

module uemu_top(
	output wire U1BLS2_p,
);

module uemu_top(
	output wire U1BHS2_p,
);

module uemu_top(
	output wire U2ALS1_p,
);

module uemu_top(
	output wire U2ALS2_p,
);

module uemu_top(
	output wire U1AHS1_p,
);

module uemu_top(
	output wire U1AHS2_p,
);

module uemu_top(
	output wire U1ALS2_p,
);

module uemu_top(
	output wire U1ALS1_p,
);



wire V2BIND;
uemu_output_channel
#(
	.DELAY_TAPS = 23
) out_V2BIND (
	.clk_core(clk_core),
	.clk_io(clk_io),
	.reset(reset),
	.signal_from_core(V2BIND),
	.deadtime(deadtime),
	.idelayctrl_rdy(idelayctrl_rdy),
	.signal_to_pin(V2BIND_p),
);

wire V2AIND;
uemu_output_channel
#(
	.DELAY_TAPS = 24
) out_V2AIND (
	.clk_core(clk_core),
	.clk_io(clk_io),
	.reset(reset),
	.signal_from_core(V2AIND),
	.deadtime(deadtime),
	.idelayctrl_rdy(idelayctrl_rdy),
	.signal_to_pin(V2AIND_p),
);

wire V3BIND;
uemu_output_channel
#(
	.DELAY_TAPS = 23
) out_V3BIND (
	.clk_core(clk_core),
	.clk_io(clk_io),
	.reset(reset),
	.signal_from_core(V3BIND),
	.deadtime(deadtime),
	.idelayctrl_rdy(idelayctrl_rdy),
	.signal_to_pin(V3BIND_p),
);

wire PMODC1;
uemu_output_channel
#(
	.DELAY_TAPS = 12
) out_PMODC1 (
	.clk_core(clk_core),
	.clk_io(clk_io),
	.reset(reset),
	.signal_from_core(PMODC1),
	.deadtime(deadtime),
	.idelayctrl_rdy(idelayctrl_rdy),
	.signal_to_pin(PMODC1_p),
);

wire V1AIND;
uemu_output_channel
#(
	.DELAY_TAPS = 22
) out_V1AIND (
	.clk_core(clk_core),
	.clk_io(clk_io),
	.reset(reset),
	.signal_from_core(V1AIND),
	.deadtime(deadtime),
	.idelayctrl_rdy(idelayctrl_rdy),
	.signal_to_pin(V1AIND_p),
);

wire V1BIND;
uemu_output_channel
#(
	.DELAY_TAPS = 22
) out_V1BIND (
	.clk_core(clk_core),
	.clk_io(clk_io),
	.reset(reset),
	.signal_from_core(V1BIND),
	.deadtime(deadtime),
	.idelayctrl_rdy(idelayctrl_rdy),
	.signal_to_pin(V1BIND_p),
);

wire PMODC5;
uemu_output_channel
#(
	.DELAY_TAPS = 12
) out_PMODC5 (
	.clk_core(clk_core),
	.clk_io(clk_io),
	.reset(reset),
	.signal_from_core(PMODC5),
	.deadtime(deadtime),
	.idelayctrl_rdy(idelayctrl_rdy),
	.signal_to_pin(PMODC5_p),
);

wire V3AIND;
uemu_output_channel
#(
	.DELAY_TAPS = 22
) out_V3AIND (
	.clk_core(clk_core),
	.clk_io(clk_io),
	.reset(reset),
	.signal_from_core(V3AIND),
	.deadtime(deadtime),
	.idelayctrl_rdy(idelayctrl_rdy),
	.signal_to_pin(V3AIND_p),
);

wire PMODC7;
uemu_output_channel
#(
	.DELAY_TAPS = 14
) out_PMODC7 (
	.clk_core(clk_core),
	.clk_io(clk_io),
	.reset(reset),
	.signal_from_core(PMODC7),
	.deadtime(deadtime),
	.idelayctrl_rdy(idelayctrl_rdy),
	.signal_to_pin(PMODC7_p),
);

wire PMODC3;
uemu_output_channel
#(
	.DELAY_TAPS = 14
) out_PMODC3 (
	.clk_core(clk_core),
	.clk_io(clk_io),
	.reset(reset),
	.signal_from_core(PMODC3),
	.deadtime(deadtime),
	.idelayctrl_rdy(idelayctrl_rdy),
	.signal_to_pin(PMODC3_p),
);

wire PMODC2;
uemu_output_channel
#(
	.DELAY_TAPS = 13
) out_PMODC2 (
	.clk_core(clk_core),
	.clk_io(clk_io),
	.reset(reset),
	.signal_from_core(PMODC2),
	.deadtime(deadtime),
	.idelayctrl_rdy(idelayctrl_rdy),
	.signal_to_pin(PMODC2_p),
);

wire PMODC0;
uemu_output_channel
#(
	.DELAY_TAPS = 12
) out_PMODC0 (
	.clk_core(clk_core),
	.clk_io(clk_io),
	.reset(reset),
	.signal_from_core(PMODC0),
	.deadtime(deadtime),
	.idelayctrl_rdy(idelayctrl_rdy),
	.signal_to_pin(PMODC0_p),
);

wire PMODC4;
uemu_output_channel
#(
	.DELAY_TAPS = 12
) out_PMODC4 (
	.clk_core(clk_core),
	.clk_io(clk_io),
	.reset(reset),
	.signal_from_core(PMODC4),
	.deadtime(deadtime),
	.idelayctrl_rdy(idelayctrl_rdy),
	.signal_to_pin(PMODC4_p),
);

wire PMODC6;
uemu_output_channel
#(
	.DELAY_TAPS = 13
) out_PMODC6 (
	.clk_core(clk_core),
	.clk_io(clk_io),
	.reset(reset),
	.signal_from_core(PMODC6),
	.deadtime(deadtime),
	.idelayctrl_rdy(idelayctrl_rdy),
	.signal_to_pin(PMODC6_p),
);

wire W1ALS1;
uemu_output_channel
#(
	.DELAY_TAPS = 24
) out_W1ALS1 (
	.clk_core(clk_core),
	.clk_io(clk_io),
	.reset(reset),
	.signal_from_core(W1ALS1),
	.deadtime(deadtime),
	.idelayctrl_rdy(idelayctrl_rdy),
	.signal_to_pin(W1ALS1_p),
);

wire FPGA_DIP2;
uemu_output_channel
#(
	.DELAY_TAPS = 9
) out_FPGA_DIP2 (
	.clk_core(clk_core),
	.clk_io(clk_io),
	.reset(reset),
	.signal_from_core(FPGA_DIP2),
	.deadtime(deadtime),
	.idelayctrl_rdy(idelayctrl_rdy),
	.signal_to_pin(FPGA_DIP2_p),
);

wire W1BLS2;
uemu_output_channel
#(
	.DELAY_TAPS = 24
) out_W1BLS2 (
	.clk_core(clk_core),
	.clk_io(clk_io),
	.reset(reset),
	.signal_from_core(W1BLS2),
	.deadtime(deadtime),
	.idelayctrl_rdy(idelayctrl_rdy),
	.signal_to_pin(W1BLS2_p),
);

wire W1AHS1;
uemu_output_channel
#(
	.DELAY_TAPS = 25
) out_W1AHS1 (
	.clk_core(clk_core),
	.clk_io(clk_io),
	.reset(reset),
	.signal_from_core(W1AHS1),
	.deadtime(deadtime),
	.idelayctrl_rdy(idelayctrl_rdy),
	.signal_to_pin(W1AHS1_p),
);

wire FPGA_DIP3;
uemu_output_channel
#(
	.DELAY_TAPS = 9
) out_FPGA_DIP3 (
	.clk_core(clk_core),
	.clk_io(clk_io),
	.reset(reset),
	.signal_from_core(FPGA_DIP3),
	.deadtime(deadtime),
	.idelayctrl_rdy(idelayctrl_rdy),
	.signal_to_pin(FPGA_DIP3_p),
);

wire PMODE6;
uemu_output_channel
#(
	.DELAY_TAPS = 12
) out_PMODE6 (
	.clk_core(clk_core),
	.clk_io(clk_io),
	.reset(reset),
	.signal_from_core(PMODE6),
	.deadtime(deadtime),
	.idelayctrl_rdy(idelayctrl_rdy),
	.signal_to_pin(PMODE6_p),
);

wire PMODE2;
uemu_output_channel
#(
	.DELAY_TAPS = 13
) out_PMODE2 (
	.clk_core(clk_core),
	.clk_io(clk_io),
	.reset(reset),
	.signal_from_core(PMODE2),
	.deadtime(deadtime),
	.idelayctrl_rdy(idelayctrl_rdy),
	.signal_to_pin(PMODE2_p),
);

wire W1BHS2;
uemu_output_channel
#(
	.DELAY_TAPS = 24
) out_W1BHS2 (
	.clk_core(clk_core),
	.clk_io(clk_io),
	.reset(reset),
	.signal_from_core(W1BHS2),
	.deadtime(deadtime),
	.idelayctrl_rdy(idelayctrl_rdy),
	.signal_to_pin(W1BHS2_p),
);

wire W2BHS1;
uemu_output_channel
#(
	.DELAY_TAPS = 24
) out_W2BHS1 (
	.clk_core(clk_core),
	.clk_io(clk_io),
	.reset(reset),
	.signal_from_core(W2BHS1),
	.deadtime(deadtime),
	.idelayctrl_rdy(idelayctrl_rdy),
	.signal_to_pin(W2BHS1_p),
);

wire W2BLS1;
uemu_output_channel
#(
	.DELAY_TAPS = 24
) out_W2BLS1 (
	.clk_core(clk_core),
	.clk_io(clk_io),
	.reset(reset),
	.signal_from_core(W2BLS1),
	.deadtime(deadtime),
	.idelayctrl_rdy(idelayctrl_rdy),
	.signal_to_pin(W2BLS1_p),
);

wire AUXSPI_MOSI;
uemu_output_channel
#(
	.DELAY_TAPS = 17
) out_AUXSPI_MOSI (
	.clk_core(clk_core),
	.clk_io(clk_io),
	.reset(reset),
	.signal_from_core(AUXSPI_MOSI),
	.deadtime(deadtime),
	.idelayctrl_rdy(idelayctrl_rdy),
	.signal_to_pin(AUXSPI_MOSI_p),
);

wire FPGA_DIP0;
uemu_output_channel
#(
	.DELAY_TAPS = 12
) out_FPGA_DIP0 (
	.clk_core(clk_core),
	.clk_io(clk_io),
	.reset(reset),
	.signal_from_core(FPGA_DIP0),
	.deadtime(deadtime),
	.idelayctrl_rdy(idelayctrl_rdy),
	.signal_to_pin(FPGA_DIP0_p),
);

wire W2ALS1;
uemu_output_channel
#(
	.DELAY_TAPS = 24
) out_W2ALS1 (
	.clk_core(clk_core),
	.clk_io(clk_io),
	.reset(reset),
	.signal_from_core(W2ALS1),
	.deadtime(deadtime),
	.idelayctrl_rdy(idelayctrl_rdy),
	.signal_to_pin(W2ALS1_p),
);

wire W2AHS1;
uemu_output_channel
#(
	.DELAY_TAPS = 24
) out_W2AHS1 (
	.clk_core(clk_core),
	.clk_io(clk_io),
	.reset(reset),
	.signal_from_core(W2AHS1),
	.deadtime(deadtime),
	.idelayctrl_rdy(idelayctrl_rdy),
	.signal_to_pin(W2AHS1_p),
);

wire AUXSPI_MISO;
uemu_output_channel
#(
	.DELAY_TAPS = 17
) out_AUXSPI_MISO (
	.clk_core(clk_core),
	.clk_io(clk_io),
	.reset(reset),
	.signal_from_core(AUXSPI_MISO),
	.deadtime(deadtime),
	.idelayctrl_rdy(idelayctrl_rdy),
	.signal_to_pin(AUXSPI_MISO_p),
);

wire W1ALS2;
uemu_output_channel
#(
	.DELAY_TAPS = 25
) out_W1ALS2 (
	.clk_core(clk_core),
	.clk_io(clk_io),
	.reset(reset),
	.signal_from_core(W1ALS2),
	.deadtime(deadtime),
	.idelayctrl_rdy(idelayctrl_rdy),
	.signal_to_pin(W1ALS2_p),
);

wire FPGA_DIP1;
uemu_output_channel
#(
	.DELAY_TAPS = 11
) out_FPGA_DIP1 (
	.clk_core(clk_core),
	.clk_io(clk_io),
	.reset(reset),
	.signal_from_core(FPGA_DIP1),
	.deadtime(deadtime),
	.idelayctrl_rdy(idelayctrl_rdy),
	.signal_to_pin(FPGA_DIP1_p),
);

wire QSPI_CLK;
uemu_output_channel
#(
	.DELAY_TAPS = 23
) out_QSPI_CLK (
	.clk_core(clk_core),
	.clk_io(clk_io),
	.reset(reset),
	.signal_from_core(QSPI_CLK),
	.deadtime(deadtime),
	.idelayctrl_rdy(idelayctrl_rdy),
	.signal_to_pin(QSPI_CLK_p),
);

wire W2ALS2;
uemu_output_channel
#(
	.DELAY_TAPS = 24
) out_W2ALS2 (
	.clk_core(clk_core),
	.clk_io(clk_io),
	.reset(reset),
	.signal_from_core(W2ALS2),
	.deadtime(deadtime),
	.idelayctrl_rdy(idelayctrl_rdy),
	.signal_to_pin(W2ALS2_p),
);

wire W2AHS2;
uemu_output_channel
#(
	.DELAY_TAPS = 24
) out_W2AHS2 (
	.clk_core(clk_core),
	.clk_io(clk_io),
	.reset(reset),
	.signal_from_core(W2AHS2),
	.deadtime(deadtime),
	.idelayctrl_rdy(idelayctrl_rdy),
	.signal_to_pin(W2AHS2_p),
);

wire W1AHS2;
uemu_output_channel
#(
	.DELAY_TAPS = 25
) out_W1AHS2 (
	.clk_core(clk_core),
	.clk_io(clk_io),
	.reset(reset),
	.signal_from_core(W1AHS2),
	.deadtime(deadtime),
	.idelayctrl_rdy(idelayctrl_rdy),
	.signal_to_pin(W1AHS2_p),
);

wire W2BHS2;
uemu_output_channel
#(
	.DELAY_TAPS = 24
) out_W2BHS2 (
	.clk_core(clk_core),
	.clk_io(clk_io),
	.reset(reset),
	.signal_from_core(W2BHS2),
	.deadtime(deadtime),
	.idelayctrl_rdy(idelayctrl_rdy),
	.signal_to_pin(W2BHS2_p),
);

wire QSPI_IO3;
uemu_output_channel
#(
	.DELAY_TAPS = 23
) out_QSPI_IO3 (
	.clk_core(clk_core),
	.clk_io(clk_io),
	.reset(reset),
	.signal_from_core(QSPI_IO3),
	.deadtime(deadtime),
	.idelayctrl_rdy(idelayctrl_rdy),
	.signal_to_pin(QSPI_IO3_p),
);

wire QSPI_IO0;
uemu_output_channel
#(
	.DELAY_TAPS = 23
) out_QSPI_IO0 (
	.clk_core(clk_core),
	.clk_io(clk_io),
	.reset(reset),
	.signal_from_core(QSPI_IO0),
	.deadtime(deadtime),
	.idelayctrl_rdy(idelayctrl_rdy),
	.signal_to_pin(QSPI_IO0_p),
);

wire AUXSPI_NSS;
uemu_output_channel
#(
	.DELAY_TAPS = 18
) out_AUXSPI_NSS (
	.clk_core(clk_core),
	.clk_io(clk_io),
	.reset(reset),
	.signal_from_core(AUXSPI_NSS),
	.deadtime(deadtime),
	.idelayctrl_rdy(idelayctrl_rdy),
	.signal_to_pin(AUXSPI_NSS_p),
);

wire AUXSPI_CLK;
uemu_output_channel
#(
	.DELAY_TAPS = 17
) out_AUXSPI_CLK (
	.clk_core(clk_core),
	.clk_io(clk_io),
	.reset(reset),
	.signal_from_core(AUXSPI_CLK),
	.deadtime(deadtime),
	.idelayctrl_rdy(idelayctrl_rdy),
	.signal_to_pin(AUXSPI_CLK_p),
);

wire W3AIND;
uemu_output_channel
#(
	.DELAY_TAPS = 25
) out_W3AIND (
	.clk_core(clk_core),
	.clk_io(clk_io),
	.reset(reset),
	.signal_from_core(W3AIND),
	.deadtime(deadtime),
	.idelayctrl_rdy(idelayctrl_rdy),
	.signal_to_pin(W3AIND_p),
);

wire PMODE7;
uemu_output_channel
#(
	.DELAY_TAPS = 12
) out_PMODE7 (
	.clk_core(clk_core),
	.clk_io(clk_io),
	.reset(reset),
	.signal_from_core(PMODE7),
	.deadtime(deadtime),
	.idelayctrl_rdy(idelayctrl_rdy),
	.signal_to_pin(PMODE7_p),
);

wire W2BLS2;
uemu_output_channel
#(
	.DELAY_TAPS = 24
) out_W2BLS2 (
	.clk_core(clk_core),
	.clk_io(clk_io),
	.reset(reset),
	.signal_from_core(W2BLS2),
	.deadtime(deadtime),
	.idelayctrl_rdy(idelayctrl_rdy),
	.signal_to_pin(W2BLS2_p),
);

wire QSPI_NCS;
uemu_output_channel
#(
	.DELAY_TAPS = 23
) out_QSPI_NCS (
	.clk_core(clk_core),
	.clk_io(clk_io),
	.reset(reset),
	.signal_from_core(QSPI_NCS),
	.deadtime(deadtime),
	.idelayctrl_rdy(idelayctrl_rdy),
	.signal_to_pin(QSPI_NCS_p),
);

wire W3ALS1;
uemu_output_channel
#(
	.DELAY_TAPS = 24
) out_W3ALS1 (
	.clk_core(clk_core),
	.clk_io(clk_io),
	.reset(reset),
	.signal_from_core(W3ALS1),
	.deadtime(deadtime),
	.idelayctrl_rdy(idelayctrl_rdy),
	.signal_to_pin(W3ALS1_p),
);

wire W3AHS1;
uemu_output_channel
#(
	.DELAY_TAPS = 24
) out_W3AHS1 (
	.clk_core(clk_core),
	.clk_io(clk_io),
	.reset(reset),
	.signal_from_core(W3AHS1),
	.deadtime(deadtime),
	.idelayctrl_rdy(idelayctrl_rdy),
	.signal_to_pin(W3AHS1_p),
);

wire W3BIND;
uemu_output_channel
#(
	.DELAY_TAPS = 25
) out_W3BIND (
	.clk_core(clk_core),
	.clk_io(clk_io),
	.reset(reset),
	.signal_from_core(W3BIND),
	.deadtime(deadtime),
	.idelayctrl_rdy(idelayctrl_rdy),
	.signal_to_pin(W3BIND_p),
);

wire PMODE3;
uemu_output_channel
#(
	.DELAY_TAPS = 13
) out_PMODE3 (
	.clk_core(clk_core),
	.clk_io(clk_io),
	.reset(reset),
	.signal_from_core(PMODE3),
	.deadtime(deadtime),
	.idelayctrl_rdy(idelayctrl_rdy),
	.signal_to_pin(PMODE3_p),
);

wire W3ALS2;
uemu_output_channel
#(
	.DELAY_TAPS = 25
) out_W3ALS2 (
	.clk_core(clk_core),
	.clk_io(clk_io),
	.reset(reset),
	.signal_from_core(W3ALS2),
	.deadtime(deadtime),
	.idelayctrl_rdy(idelayctrl_rdy),
	.signal_to_pin(W3ALS2_p),
);

wire W1BLS1;
uemu_output_channel
#(
	.DELAY_TAPS = 25
) out_W1BLS1 (
	.clk_core(clk_core),
	.clk_io(clk_io),
	.reset(reset),
	.signal_from_core(W1BLS1),
	.deadtime(deadtime),
	.idelayctrl_rdy(idelayctrl_rdy),
	.signal_to_pin(W1BLS1_p),
);

wire W3BHS1;
uemu_output_channel
#(
	.DELAY_TAPS = 24
) out_W3BHS1 (
	.clk_core(clk_core),
	.clk_io(clk_io),
	.reset(reset),
	.signal_from_core(W3BHS1),
	.deadtime(deadtime),
	.idelayctrl_rdy(idelayctrl_rdy),
	.signal_to_pin(W3BHS1_p),
);

wire W3BLS1;
uemu_output_channel
#(
	.DELAY_TAPS = 24
) out_W3BLS1 (
	.clk_core(clk_core),
	.clk_io(clk_io),
	.reset(reset),
	.signal_from_core(W3BLS1),
	.deadtime(deadtime),
	.idelayctrl_rdy(idelayctrl_rdy),
	.signal_to_pin(W3BLS1_p),
);

wire QSPI_IO2;
uemu_output_channel
#(
	.DELAY_TAPS = 24
) out_QSPI_IO2 (
	.clk_core(clk_core),
	.clk_io(clk_io),
	.reset(reset),
	.signal_from_core(QSPI_IO2),
	.deadtime(deadtime),
	.idelayctrl_rdy(idelayctrl_rdy),
	.signal_to_pin(QSPI_IO2_p),
);

wire QSPI_IO1;
uemu_output_channel
#(
	.DELAY_TAPS = 24
) out_QSPI_IO1 (
	.clk_core(clk_core),
	.clk_io(clk_io),
	.reset(reset),
	.signal_from_core(QSPI_IO1),
	.deadtime(deadtime),
	.idelayctrl_rdy(idelayctrl_rdy),
	.signal_to_pin(QSPI_IO1_p),
);

wire W2BIND;
uemu_output_channel
#(
	.DELAY_TAPS = 25
) out_W2BIND (
	.clk_core(clk_core),
	.clk_io(clk_io),
	.reset(reset),
	.signal_from_core(W2BIND),
	.deadtime(deadtime),
	.idelayctrl_rdy(idelayctrl_rdy),
	.signal_to_pin(W2BIND_p),
);

wire W3AHS2;
uemu_output_channel
#(
	.DELAY_TAPS = 25
) out_W3AHS2 (
	.clk_core(clk_core),
	.clk_io(clk_io),
	.reset(reset),
	.signal_from_core(W3AHS2),
	.deadtime(deadtime),
	.idelayctrl_rdy(idelayctrl_rdy),
	.signal_to_pin(W3AHS2_p),
);

wire W1BHS1;
uemu_output_channel
#(
	.DELAY_TAPS = 26
) out_W1BHS1 (
	.clk_core(clk_core),
	.clk_io(clk_io),
	.reset(reset),
	.signal_from_core(W1BHS1),
	.deadtime(deadtime),
	.idelayctrl_rdy(idelayctrl_rdy),
	.signal_to_pin(W1BHS1_p),
);

wire W1BIND;
uemu_output_channel
#(
	.DELAY_TAPS = 25
) out_W1BIND (
	.clk_core(clk_core),
	.clk_io(clk_io),
	.reset(reset),
	.signal_from_core(W1BIND),
	.deadtime(deadtime),
	.idelayctrl_rdy(idelayctrl_rdy),
	.signal_to_pin(W1BIND_p),
);

wire W1AIND;
uemu_output_channel
#(
	.DELAY_TAPS = 25
) out_W1AIND (
	.clk_core(clk_core),
	.clk_io(clk_io),
	.reset(reset),
	.signal_from_core(W1AIND),
	.deadtime(deadtime),
	.idelayctrl_rdy(idelayctrl_rdy),
	.signal_to_pin(W1AIND_p),
);

wire W3BHS2;
uemu_output_channel
#(
	.DELAY_TAPS = 25
) out_W3BHS2 (
	.clk_core(clk_core),
	.clk_io(clk_io),
	.reset(reset),
	.signal_from_core(W3BHS2),
	.deadtime(deadtime),
	.idelayctrl_rdy(idelayctrl_rdy),
	.signal_to_pin(W3BHS2_p),
);

wire W3BLS2;
uemu_output_channel
#(
	.DELAY_TAPS = 25
) out_W3BLS2 (
	.clk_core(clk_core),
	.clk_io(clk_io),
	.reset(reset),
	.signal_from_core(W3BLS2),
	.deadtime(deadtime),
	.idelayctrl_rdy(idelayctrl_rdy),
	.signal_to_pin(W3BLS2_p),
);

wire W2AIND;
uemu_output_channel
#(
	.DELAY_TAPS = 26
) out_W2AIND (
	.clk_core(clk_core),
	.clk_io(clk_io),
	.reset(reset),
	.signal_from_core(W2AIND),
	.deadtime(deadtime),
	.idelayctrl_rdy(idelayctrl_rdy),
	.signal_to_pin(W2AIND_p),
);

wire PMODE0;
uemu_output_channel
#(
	.DELAY_TAPS = 14
) out_PMODE0 (
	.clk_core(clk_core),
	.clk_io(clk_io),
	.reset(reset),
	.signal_from_core(PMODE0),
	.deadtime(deadtime),
	.idelayctrl_rdy(idelayctrl_rdy),
	.signal_to_pin(PMODE0_p),
);

wire PMODD3;
uemu_output_channel
#(
	.DELAY_TAPS = 13
) out_PMODD3 (
	.clk_core(clk_core),
	.clk_io(clk_io),
	.reset(reset),
	.signal_from_core(PMODD3),
	.deadtime(deadtime),
	.idelayctrl_rdy(idelayctrl_rdy),
	.signal_to_pin(PMODD3_p),
);

wire PMODD7;
uemu_output_channel
#(
	.DELAY_TAPS = 13
) out_PMODD7 (
	.clk_core(clk_core),
	.clk_io(clk_io),
	.reset(reset),
	.signal_from_core(PMODD7),
	.deadtime(deadtime),
	.idelayctrl_rdy(idelayctrl_rdy),
	.signal_to_pin(PMODD7_p),
);

wire PMODD0;
uemu_output_channel
#(
	.DELAY_TAPS = 13
) out_PMODD0 (
	.clk_core(clk_core),
	.clk_io(clk_io),
	.reset(reset),
	.signal_from_core(PMODD0),
	.deadtime(deadtime),
	.idelayctrl_rdy(idelayctrl_rdy),
	.signal_to_pin(PMODD0_p),
);

wire PMODD4;
uemu_output_channel
#(
	.DELAY_TAPS = 13
) out_PMODD4 (
	.clk_core(clk_core),
	.clk_io(clk_io),
	.reset(reset),
	.signal_from_core(PMODD4),
	.deadtime(deadtime),
	.idelayctrl_rdy(idelayctrl_rdy),
	.signal_to_pin(PMODD4_p),
);

wire BTN_DIRW;
uemu_output_channel
#(
	.DELAY_TAPS = 8
) out_BTN_DIRW (
	.clk_core(clk_core),
	.clk_io(clk_io),
	.reset(reset),
	.signal_from_core(BTN_DIRW),
	.deadtime(deadtime),
	.idelayctrl_rdy(idelayctrl_rdy),
	.signal_to_pin(BTN_DIRW_p),
);

wire PMODE4;
uemu_output_channel
#(
	.DELAY_TAPS = 14
) out_PMODE4 (
	.clk_core(clk_core),
	.clk_io(clk_io),
	.reset(reset),
	.signal_from_core(PMODE4),
	.deadtime(deadtime),
	.idelayctrl_rdy(idelayctrl_rdy),
	.signal_to_pin(PMODE4_p),
);

wire V1ALS2;
uemu_output_channel
#(
	.DELAY_TAPS = 26
) out_V1ALS2 (
	.clk_core(clk_core),
	.clk_io(clk_io),
	.reset(reset),
	.signal_from_core(V1ALS2),
	.deadtime(deadtime),
	.idelayctrl_rdy(idelayctrl_rdy),
	.signal_to_pin(V1ALS2_p),
);

wire V1ALS1;
uemu_output_channel
#(
	.DELAY_TAPS = 26
) out_V1ALS1 (
	.clk_core(clk_core),
	.clk_io(clk_io),
	.reset(reset),
	.signal_from_core(V1ALS1),
	.deadtime(deadtime),
	.idelayctrl_rdy(idelayctrl_rdy),
	.signal_to_pin(V1ALS1_p),
);

wire V1AHS1;
uemu_output_channel
#(
	.DELAY_TAPS = 26
) out_V1AHS1 (
	.clk_core(clk_core),
	.clk_io(clk_io),
	.reset(reset),
	.signal_from_core(V1AHS1),
	.deadtime(deadtime),
	.idelayctrl_rdy(idelayctrl_rdy),
	.signal_to_pin(V1AHS1_p),
);

wire FPGA_LED2;
uemu_output_channel
#(
	.DELAY_TAPS = 0
) out_FPGA_LED2 (
	.clk_core(clk_core),
	.clk_io(clk_io),
	.reset(reset),
	.signal_from_core(FPGA_LED2),
	.deadtime(deadtime),
	.idelayctrl_rdy(idelayctrl_rdy),
	.signal_to_pin(FPGA_LED2_p),
);

wire FPGA_LED1;
uemu_output_channel
#(
	.DELAY_TAPS = 0
) out_FPGA_LED1 (
	.clk_core(clk_core),
	.clk_io(clk_io),
	.reset(reset),
	.signal_from_core(FPGA_LED1),
	.deadtime(deadtime),
	.idelayctrl_rdy(idelayctrl_rdy),
	.signal_to_pin(FPGA_LED1_p),
);

wire BTN_DIRS;
uemu_output_channel
#(
	.DELAY_TAPS = 5
) out_BTN_DIRS (
	.clk_core(clk_core),
	.clk_io(clk_io),
	.reset(reset),
	.signal_from_core(BTN_DIRS),
	.deadtime(deadtime),
	.idelayctrl_rdy(idelayctrl_rdy),
	.signal_to_pin(BTN_DIRS_p),
);

wire V1AHS2;
uemu_output_channel
#(
	.DELAY_TAPS = 26
) out_V1AHS2 (
	.clk_core(clk_core),
	.clk_io(clk_io),
	.reset(reset),
	.signal_from_core(V1AHS2),
	.deadtime(deadtime),
	.idelayctrl_rdy(idelayctrl_rdy),
	.signal_to_pin(V1AHS2_p),
);

wire PMODE1;
uemu_output_channel
#(
	.DELAY_TAPS = 15
) out_PMODE1 (
	.clk_core(clk_core),
	.clk_io(clk_io),
	.reset(reset),
	.signal_from_core(PMODE1),
	.deadtime(deadtime),
	.idelayctrl_rdy(idelayctrl_rdy),
	.signal_to_pin(PMODE1_p),
);

wire V1BHS1;
uemu_output_channel
#(
	.DELAY_TAPS = 26
) out_V1BHS1 (
	.clk_core(clk_core),
	.clk_io(clk_io),
	.reset(reset),
	.signal_from_core(V1BHS1),
	.deadtime(deadtime),
	.idelayctrl_rdy(idelayctrl_rdy),
	.signal_to_pin(V1BHS1_p),
);

wire V1BLS1;
uemu_output_channel
#(
	.DELAY_TAPS = 26
) out_V1BLS1 (
	.clk_core(clk_core),
	.clk_io(clk_io),
	.reset(reset),
	.signal_from_core(V1BLS1),
	.deadtime(deadtime),
	.idelayctrl_rdy(idelayctrl_rdy),
	.signal_to_pin(V1BLS1_p),
);

wire FPGA_LED3;
uemu_output_channel
#(
	.DELAY_TAPS = 0
) out_FPGA_LED3 (
	.clk_core(clk_core),
	.clk_io(clk_io),
	.reset(reset),
	.signal_from_core(FPGA_LED3),
	.deadtime(deadtime),
	.idelayctrl_rdy(idelayctrl_rdy),
	.signal_to_pin(FPGA_LED3_p),
);

wire FPGA_LED4;
uemu_output_channel
#(
	.DELAY_TAPS = 0
) out_FPGA_LED4 (
	.clk_core(clk_core),
	.clk_io(clk_io),
	.reset(reset),
	.signal_from_core(FPGA_LED4),
	.deadtime(deadtime),
	.idelayctrl_rdy(idelayctrl_rdy),
	.signal_to_pin(FPGA_LED4_p),
);

wire PMODE5;
uemu_output_channel
#(
	.DELAY_TAPS = 15
) out_PMODE5 (
	.clk_core(clk_core),
	.clk_io(clk_io),
	.reset(reset),
	.signal_from_core(PMODE5),
	.deadtime(deadtime),
	.idelayctrl_rdy(idelayctrl_rdy),
	.signal_to_pin(PMODE5_p),
);

wire V1BHS2;
uemu_output_channel
#(
	.DELAY_TAPS = 26
) out_V1BHS2 (
	.clk_core(clk_core),
	.clk_io(clk_io),
	.reset(reset),
	.signal_from_core(V1BHS2),
	.deadtime(deadtime),
	.idelayctrl_rdy(idelayctrl_rdy),
	.signal_to_pin(V1BHS2_p),
);

wire V3BHS2;
uemu_output_channel
#(
	.DELAY_TAPS = 25
) out_V3BHS2 (
	.clk_core(clk_core),
	.clk_io(clk_io),
	.reset(reset),
	.signal_from_core(V3BHS2),
	.deadtime(deadtime),
	.idelayctrl_rdy(idelayctrl_rdy),
	.signal_to_pin(V3BHS2_p),
);

wire V2ALS1;
uemu_output_channel
#(
	.DELAY_TAPS = 26
) out_V2ALS1 (
	.clk_core(clk_core),
	.clk_io(clk_io),
	.reset(reset),
	.signal_from_core(V2ALS1),
	.deadtime(deadtime),
	.idelayctrl_rdy(idelayctrl_rdy),
	.signal_to_pin(V2ALS1_p),
);

wire V2AHS1;
uemu_output_channel
#(
	.DELAY_TAPS = 26
) out_V2AHS1 (
	.clk_core(clk_core),
	.clk_io(clk_io),
	.reset(reset),
	.signal_from_core(V2AHS1),
	.deadtime(deadtime),
	.idelayctrl_rdy(idelayctrl_rdy),
	.signal_to_pin(V2AHS1_p),
);

wire V1BLS2;
uemu_output_channel
#(
	.DELAY_TAPS = 26
) out_V1BLS2 (
	.clk_core(clk_core),
	.clk_io(clk_io),
	.reset(reset),
	.signal_from_core(V1BLS2),
	.deadtime(deadtime),
	.idelayctrl_rdy(idelayctrl_rdy),
	.signal_to_pin(V1BLS2_p),
);

wire V3BLS2;
uemu_output_channel
#(
	.DELAY_TAPS = 25
) out_V3BLS2 (
	.clk_core(clk_core),
	.clk_io(clk_io),
	.reset(reset),
	.signal_from_core(V3BLS2),
	.deadtime(deadtime),
	.idelayctrl_rdy(idelayctrl_rdy),
	.signal_to_pin(V3BLS2_p),
);

wire BTN_DIRN;
uemu_output_channel
#(
	.DELAY_TAPS = 6
) out_BTN_DIRN (
	.clk_core(clk_core),
	.clk_io(clk_io),
	.reset(reset),
	.signal_from_core(BTN_DIRN),
	.deadtime(deadtime),
	.idelayctrl_rdy(idelayctrl_rdy),
	.signal_to_pin(BTN_DIRN_p),
);

wire BTN_DIRE;
uemu_output_channel
#(
	.DELAY_TAPS = 4
) out_BTN_DIRE (
	.clk_core(clk_core),
	.clk_io(clk_io),
	.reset(reset),
	.signal_from_core(BTN_DIRE),
	.deadtime(deadtime),
	.idelayctrl_rdy(idelayctrl_rdy),
	.signal_to_pin(BTN_DIRE_p),
);

wire PMODD2;
uemu_output_channel
#(
	.DELAY_TAPS = 14
) out_PMODD2 (
	.clk_core(clk_core),
	.clk_io(clk_io),
	.reset(reset),
	.signal_from_core(PMODD2),
	.deadtime(deadtime),
	.idelayctrl_rdy(idelayctrl_rdy),
	.signal_to_pin(PMODD2_p),
);

wire PMODD6;
uemu_output_channel
#(
	.DELAY_TAPS = 14
) out_PMODD6 (
	.clk_core(clk_core),
	.clk_io(clk_io),
	.reset(reset),
	.signal_from_core(PMODD6),
	.deadtime(deadtime),
	.idelayctrl_rdy(idelayctrl_rdy),
	.signal_to_pin(PMODD6_p),
);

wire V3ALS1;
uemu_output_channel
#(
	.DELAY_TAPS = 26
) out_V3ALS1 (
	.clk_core(clk_core),
	.clk_io(clk_io),
	.reset(reset),
	.signal_from_core(V3ALS1),
	.deadtime(deadtime),
	.idelayctrl_rdy(idelayctrl_rdy),
	.signal_to_pin(V3ALS1_p),
);

wire BTN_DIRC;
uemu_output_channel
#(
	.DELAY_TAPS = 7
) out_BTN_DIRC (
	.clk_core(clk_core),
	.clk_io(clk_io),
	.reset(reset),
	.signal_from_core(BTN_DIRC),
	.deadtime(deadtime),
	.idelayctrl_rdy(idelayctrl_rdy),
	.signal_to_pin(BTN_DIRC_p),
);

wire PMODD5;
uemu_output_channel
#(
	.DELAY_TAPS = 14
) out_PMODD5 (
	.clk_core(clk_core),
	.clk_io(clk_io),
	.reset(reset),
	.signal_from_core(PMODD5),
	.deadtime(deadtime),
	.idelayctrl_rdy(idelayctrl_rdy),
	.signal_to_pin(PMODD5_p),
);

wire V2ALS2;
uemu_output_channel
#(
	.DELAY_TAPS = 26
) out_V2ALS2 (
	.clk_core(clk_core),
	.clk_io(clk_io),
	.reset(reset),
	.signal_from_core(V2ALS2),
	.deadtime(deadtime),
	.idelayctrl_rdy(idelayctrl_rdy),
	.signal_to_pin(V2ALS2_p),
);

wire V3AHS1;
uemu_output_channel
#(
	.DELAY_TAPS = 26
) out_V3AHS1 (
	.clk_core(clk_core),
	.clk_io(clk_io),
	.reset(reset),
	.signal_from_core(V3AHS1),
	.deadtime(deadtime),
	.idelayctrl_rdy(idelayctrl_rdy),
	.signal_to_pin(V3AHS1_p),
);

wire BTN_DIRB;
uemu_output_channel
#(
	.DELAY_TAPS = 2
) out_BTN_DIRB (
	.clk_core(clk_core),
	.clk_io(clk_io),
	.reset(reset),
	.signal_from_core(BTN_DIRB),
	.deadtime(deadtime),
	.idelayctrl_rdy(idelayctrl_rdy),
	.signal_to_pin(BTN_DIRB_p),
);

wire V3ALS2;
uemu_output_channel
#(
	.DELAY_TAPS = 26
) out_V3ALS2 (
	.clk_core(clk_core),
	.clk_io(clk_io),
	.reset(reset),
	.signal_from_core(V3ALS2),
	.deadtime(deadtime),
	.idelayctrl_rdy(idelayctrl_rdy),
	.signal_to_pin(V3ALS2_p),
);

wire PMODD1;
uemu_output_channel
#(
	.DELAY_TAPS = 14
) out_PMODD1 (
	.clk_core(clk_core),
	.clk_io(clk_io),
	.reset(reset),
	.signal_from_core(PMODD1),
	.deadtime(deadtime),
	.idelayctrl_rdy(idelayctrl_rdy),
	.signal_to_pin(PMODD1_p),
);

wire V2AHS2;
uemu_output_channel
#(
	.DELAY_TAPS = 26
) out_V2AHS2 (
	.clk_core(clk_core),
	.clk_io(clk_io),
	.reset(reset),
	.signal_from_core(V2AHS2),
	.deadtime(deadtime),
	.idelayctrl_rdy(idelayctrl_rdy),
	.signal_to_pin(V2AHS2_p),
);

wire V2BHS2;
uemu_output_channel
#(
	.DELAY_TAPS = 26
) out_V2BHS2 (
	.clk_core(clk_core),
	.clk_io(clk_io),
	.reset(reset),
	.signal_from_core(V2BHS2),
	.deadtime(deadtime),
	.idelayctrl_rdy(idelayctrl_rdy),
	.signal_to_pin(V2BHS2_p),
);

wire V3AHS2;
uemu_output_channel
#(
	.DELAY_TAPS = 26
) out_V3AHS2 (
	.clk_core(clk_core),
	.clk_io(clk_io),
	.reset(reset),
	.signal_from_core(V3AHS2),
	.deadtime(deadtime),
	.idelayctrl_rdy(idelayctrl_rdy),
	.signal_to_pin(V3AHS2_p),
);

wire V2BHS1;
uemu_output_channel
#(
	.DELAY_TAPS = 26
) out_V2BHS1 (
	.clk_core(clk_core),
	.clk_io(clk_io),
	.reset(reset),
	.signal_from_core(V2BHS1),
	.deadtime(deadtime),
	.idelayctrl_rdy(idelayctrl_rdy),
	.signal_to_pin(V2BHS1_p),
);

wire V3BHS1;
uemu_output_channel
#(
	.DELAY_TAPS = 26
) out_V3BHS1 (
	.clk_core(clk_core),
	.clk_io(clk_io),
	.reset(reset),
	.signal_from_core(V3BHS1),
	.deadtime(deadtime),
	.idelayctrl_rdy(idelayctrl_rdy),
	.signal_to_pin(V3BHS1_p),
);

wire V3BLS1;
uemu_output_channel
#(
	.DELAY_TAPS = 26
) out_V3BLS1 (
	.clk_core(clk_core),
	.clk_io(clk_io),
	.reset(reset),
	.signal_from_core(V3BLS1),
	.deadtime(deadtime),
	.idelayctrl_rdy(idelayctrl_rdy),
	.signal_to_pin(V3BLS1_p),
);

wire V2BLS2;
uemu_output_channel
#(
	.DELAY_TAPS = 26
) out_V2BLS2 (
	.clk_core(clk_core),
	.clk_io(clk_io),
	.reset(reset),
	.signal_from_core(V2BLS2),
	.deadtime(deadtime),
	.idelayctrl_rdy(idelayctrl_rdy),
	.signal_to_pin(V2BLS2_p),
);

wire V2BLS1;
uemu_output_channel
#(
	.DELAY_TAPS = 26
) out_V2BLS1 (
	.clk_core(clk_core),
	.clk_io(clk_io),
	.reset(reset),
	.signal_from_core(V2BLS1),
	.deadtime(deadtime),
	.idelayctrl_rdy(idelayctrl_rdy),
	.signal_to_pin(V2BLS1_p),
);

wire U3BIND;
uemu_output_channel
#(
	.DELAY_TAPS = 27
) out_U3BIND (
	.clk_core(clk_core),
	.clk_io(clk_io),
	.reset(reset),
	.signal_from_core(U3BIND),
	.deadtime(deadtime),
	.idelayctrl_rdy(idelayctrl_rdy),
	.signal_to_pin(U3BIND_p),
);

wire U1AIND;
uemu_output_channel
#(
	.DELAY_TAPS = 26
) out_U1AIND (
	.clk_core(clk_core),
	.clk_io(clk_io),
	.reset(reset),
	.signal_from_core(U1AIND),
	.deadtime(deadtime),
	.idelayctrl_rdy(idelayctrl_rdy),
	.signal_to_pin(U1AIND_p),
);

wire U1BIND;
uemu_output_channel
#(
	.DELAY_TAPS = 26
) out_U1BIND (
	.clk_core(clk_core),
	.clk_io(clk_io),
	.reset(reset),
	.signal_from_core(U1BIND),
	.deadtime(deadtime),
	.idelayctrl_rdy(idelayctrl_rdy),
	.signal_to_pin(U1BIND_p),
);

wire U3BLS2;
uemu_output_channel
#(
	.DELAY_TAPS = 26
) out_U3BLS2 (
	.clk_core(clk_core),
	.clk_io(clk_io),
	.reset(reset),
	.signal_from_core(U3BLS2),
	.deadtime(deadtime),
	.idelayctrl_rdy(idelayctrl_rdy),
	.signal_to_pin(U3BLS2_p),
);

wire U3BHS2;
uemu_output_channel
#(
	.DELAY_TAPS = 26
) out_U3BHS2 (
	.clk_core(clk_core),
	.clk_io(clk_io),
	.reset(reset),
	.signal_from_core(U3BHS2),
	.deadtime(deadtime),
	.idelayctrl_rdy(idelayctrl_rdy),
	.signal_to_pin(U3BHS2_p),
);

wire U3AIND;
uemu_output_channel
#(
	.DELAY_TAPS = 27
) out_U3AIND (
	.clk_core(clk_core),
	.clk_io(clk_io),
	.reset(reset),
	.signal_from_core(U3AIND),
	.deadtime(deadtime),
	.idelayctrl_rdy(idelayctrl_rdy),
	.signal_to_pin(U3AIND_p),
);

wire U2AIND;
uemu_output_channel
#(
	.DELAY_TAPS = 26
) out_U2AIND (
	.clk_core(clk_core),
	.clk_io(clk_io),
	.reset(reset),
	.signal_from_core(U2AIND),
	.deadtime(deadtime),
	.idelayctrl_rdy(idelayctrl_rdy),
	.signal_to_pin(U2AIND_p),
);

wire U2BIND;
uemu_output_channel
#(
	.DELAY_TAPS = 26
) out_U2BIND (
	.clk_core(clk_core),
	.clk_io(clk_io),
	.reset(reset),
	.signal_from_core(U2BIND),
	.deadtime(deadtime),
	.idelayctrl_rdy(idelayctrl_rdy),
	.signal_to_pin(U2BIND_p),
);

wire U2BLS2;
uemu_output_channel
#(
	.DELAY_TAPS = 27
) out_U2BLS2 (
	.clk_core(clk_core),
	.clk_io(clk_io),
	.reset(reset),
	.signal_from_core(U2BLS2),
	.deadtime(deadtime),
	.idelayctrl_rdy(idelayctrl_rdy),
	.signal_to_pin(U2BLS2_p),
);

wire U3BLS1;
uemu_output_channel
#(
	.DELAY_TAPS = 26
) out_U3BLS1 (
	.clk_core(clk_core),
	.clk_io(clk_io),
	.reset(reset),
	.signal_from_core(U3BLS1),
	.deadtime(deadtime),
	.idelayctrl_rdy(idelayctrl_rdy),
	.signal_to_pin(U3BLS1_p),
);

wire U3BHS1;
uemu_output_channel
#(
	.DELAY_TAPS = 26
) out_U3BHS1 (
	.clk_core(clk_core),
	.clk_io(clk_io),
	.reset(reset),
	.signal_from_core(U3BHS1),
	.deadtime(deadtime),
	.idelayctrl_rdy(idelayctrl_rdy),
	.signal_to_pin(U3BHS1_p),
);

wire U2BHS2;
uemu_output_channel
#(
	.DELAY_TAPS = 27
) out_U2BHS2 (
	.clk_core(clk_core),
	.clk_io(clk_io),
	.reset(reset),
	.signal_from_core(U2BHS2),
	.deadtime(deadtime),
	.idelayctrl_rdy(idelayctrl_rdy),
	.signal_to_pin(U2BHS2_p),
);

wire U3ALS2;
uemu_output_channel
#(
	.DELAY_TAPS = 27
) out_U3ALS2 (
	.clk_core(clk_core),
	.clk_io(clk_io),
	.reset(reset),
	.signal_from_core(U3ALS2),
	.deadtime(deadtime),
	.idelayctrl_rdy(idelayctrl_rdy),
	.signal_to_pin(U3ALS2_p),
);

wire U3AHS2;
uemu_output_channel
#(
	.DELAY_TAPS = 27
) out_U3AHS2 (
	.clk_core(clk_core),
	.clk_io(clk_io),
	.reset(reset),
	.signal_from_core(U3AHS2),
	.deadtime(deadtime),
	.idelayctrl_rdy(idelayctrl_rdy),
	.signal_to_pin(U3AHS2_p),
);

wire U3ALS1;
uemu_output_channel
#(
	.DELAY_TAPS = 26
) out_U3ALS1 (
	.clk_core(clk_core),
	.clk_io(clk_io),
	.reset(reset),
	.signal_from_core(U3ALS1),
	.deadtime(deadtime),
	.idelayctrl_rdy(idelayctrl_rdy),
	.signal_to_pin(U3ALS1_p),
);

wire U1BLS1;
uemu_output_channel
#(
	.DELAY_TAPS = 26
) out_U1BLS1 (
	.clk_core(clk_core),
	.clk_io(clk_io),
	.reset(reset),
	.signal_from_core(U1BLS1),
	.deadtime(deadtime),
	.idelayctrl_rdy(idelayctrl_rdy),
	.signal_to_pin(U1BLS1_p),
);

wire U2BLS1;
uemu_output_channel
#(
	.DELAY_TAPS = 27
) out_U2BLS1 (
	.clk_core(clk_core),
	.clk_io(clk_io),
	.reset(reset),
	.signal_from_core(U2BLS1),
	.deadtime(deadtime),
	.idelayctrl_rdy(idelayctrl_rdy),
	.signal_to_pin(U2BLS1_p),
);

wire U3AHS1;
uemu_output_channel
#(
	.DELAY_TAPS = 26
) out_U3AHS1 (
	.clk_core(clk_core),
	.clk_io(clk_io),
	.reset(reset),
	.signal_from_core(U3AHS1),
	.deadtime(deadtime),
	.idelayctrl_rdy(idelayctrl_rdy),
	.signal_to_pin(U3AHS1_p),
);

wire U1BHS1;
uemu_output_channel
#(
	.DELAY_TAPS = 26
) out_U1BHS1 (
	.clk_core(clk_core),
	.clk_io(clk_io),
	.reset(reset),
	.signal_from_core(U1BHS1),
	.deadtime(deadtime),
	.idelayctrl_rdy(idelayctrl_rdy),
	.signal_to_pin(U1BHS1_p),
);

wire U2BHS1;
uemu_output_channel
#(
	.DELAY_TAPS = 27
) out_U2BHS1 (
	.clk_core(clk_core),
	.clk_io(clk_io),
	.reset(reset),
	.signal_from_core(U2BHS1),
	.deadtime(deadtime),
	.idelayctrl_rdy(idelayctrl_rdy),
	.signal_to_pin(U2BHS1_p),
);

wire U2AHS1;
uemu_output_channel
#(
	.DELAY_TAPS = 27
) out_U2AHS1 (
	.clk_core(clk_core),
	.clk_io(clk_io),
	.reset(reset),
	.signal_from_core(U2AHS1),
	.deadtime(deadtime),
	.idelayctrl_rdy(idelayctrl_rdy),
	.signal_to_pin(U2AHS1_p),
);

wire U2AHS2;
uemu_output_channel
#(
	.DELAY_TAPS = 27
) out_U2AHS2 (
	.clk_core(clk_core),
	.clk_io(clk_io),
	.reset(reset),
	.signal_from_core(U2AHS2),
	.deadtime(deadtime),
	.idelayctrl_rdy(idelayctrl_rdy),
	.signal_to_pin(U2AHS2_p),
);

wire U1BLS2;
uemu_output_channel
#(
	.DELAY_TAPS = 27
) out_U1BLS2 (
	.clk_core(clk_core),
	.clk_io(clk_io),
	.reset(reset),
	.signal_from_core(U1BLS2),
	.deadtime(deadtime),
	.idelayctrl_rdy(idelayctrl_rdy),
	.signal_to_pin(U1BLS2_p),
);

wire U1BHS2;
uemu_output_channel
#(
	.DELAY_TAPS = 27
) out_U1BHS2 (
	.clk_core(clk_core),
	.clk_io(clk_io),
	.reset(reset),
	.signal_from_core(U1BHS2),
	.deadtime(deadtime),
	.idelayctrl_rdy(idelayctrl_rdy),
	.signal_to_pin(U1BHS2_p),
);

wire U2ALS1;
uemu_output_channel
#(
	.DELAY_TAPS = 27
) out_U2ALS1 (
	.clk_core(clk_core),
	.clk_io(clk_io),
	.reset(reset),
	.signal_from_core(U2ALS1),
	.deadtime(deadtime),
	.idelayctrl_rdy(idelayctrl_rdy),
	.signal_to_pin(U2ALS1_p),
);

wire U2ALS2;
uemu_output_channel
#(
	.DELAY_TAPS = 27
) out_U2ALS2 (
	.clk_core(clk_core),
	.clk_io(clk_io),
	.reset(reset),
	.signal_from_core(U2ALS2),
	.deadtime(deadtime),
	.idelayctrl_rdy(idelayctrl_rdy),
	.signal_to_pin(U2ALS2_p),
);

wire U1AHS1;
uemu_output_channel
#(
	.DELAY_TAPS = 27
) out_U1AHS1 (
	.clk_core(clk_core),
	.clk_io(clk_io),
	.reset(reset),
	.signal_from_core(U1AHS1),
	.deadtime(deadtime),
	.idelayctrl_rdy(idelayctrl_rdy),
	.signal_to_pin(U1AHS1_p),
);

wire U1AHS2;
uemu_output_channel
#(
	.DELAY_TAPS = 27
) out_U1AHS2 (
	.clk_core(clk_core),
	.clk_io(clk_io),
	.reset(reset),
	.signal_from_core(U1AHS2),
	.deadtime(deadtime),
	.idelayctrl_rdy(idelayctrl_rdy),
	.signal_to_pin(U1AHS2_p),
);

wire U1ALS2;
uemu_output_channel
#(
	.DELAY_TAPS = 27
) out_U1ALS2 (
	.clk_core(clk_core),
	.clk_io(clk_io),
	.reset(reset),
	.signal_from_core(U1ALS2),
	.deadtime(deadtime),
	.idelayctrl_rdy(idelayctrl_rdy),
	.signal_to_pin(U1ALS2_p),
);

wire U1ALS1;
uemu_output_channel
#(
	.DELAY_TAPS = 27
) out_U1ALS1 (
	.clk_core(clk_core),
	.clk_io(clk_io),
	.reset(reset),
	.signal_from_core(U1ALS1),
	.deadtime(deadtime),
	.idelayctrl_rdy(idelayctrl_rdy),
	.signal_to_pin(U1ALS1_p),
);

