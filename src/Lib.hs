{-# OPTIONS_GHC -Wall #-}
{-# LANGUAGE TypeApplications #-}

module Lib where

import qualified Data.Map        as M
import           Data.List.Split      (splitOn)
import           Data.List            (intercalate)

convert :: String -> Int -> (String,Int)
convert component pinNumber | odd pinNumber = (idx, pinNumber + 1)
                            | otherwise     = (idx, pinNumber - 1)
  where
    idx = "JB" ++ [last component]

wireStuff :: Float -> Float -> [Float]
-- wireStuff 0 _ = [0.0,0.0]
-- wireStuff _ 0 = [0.0,0.0]
wireStuff m c = [len, delay]
  where
    len   = (m + c) * 1.0e-3
    delay = len / (2 * 1.0e8 )

genVerilog :: [String] -> (String,String,String)
genVerilog [ pin,_,_,_,_,net,_,_,_,_,taps] = (line1,line2,line3)
  where 
    line1 = "set_property -dict { PACKAGE_PIN " ++ pin
         ++ " IOSTANDARD LVCMOS33 } [get_ports { " ++ net ++ "_p }];"
    line2 = "module uemu_top(\n\toutput wire " ++ net ++ "_p,\n);\n"
    line3 = "wire " ++ net ++ ";\n"
         ++ "uemu_output_channel\n"
         ++ "#(\n"
         ++ "\t.DELAY_TAPS = " ++ taps ++ "\n"
         ++ ") out_" ++ net ++ " (\n"
         ++ "\t.clk_core(clk_core),\n"
         ++ "\t.clk_io(clk_io),\n"
         ++ "\t.reset(reset),\n"
         ++ "\t.signal_from_core(" ++ net ++ "),\n"
         ++ "\t.deadtime(deadtime),\n"
         ++ "\t.idelayctrl_rdy(idelayctrl_rdy),\n"
         ++ "\t.signal_to_pin(" ++ net ++ "_p),\n"
         ++ ");\n"

mapPins :: IO ()
mapPins = do
    fpgaNets <- map ((\[a,b] -> (a,b)) . splitOn ",") . drop 1 . lines
                <$> readFile "./rsc/fpganets.csv"
    moduleNets <- map ((\[a,b,c] -> (a, read @Int b, c)) . splitOn ",") . drop 1 . lines
                <$> readFile "./rsc/modulenets.csv"
    carrierNets <- map ((\[a,b,c] -> (a, b, read @Int c)) . splitOn ",") . drop 1 . lines
                <$> readFile "./rsc/carriernets.csv"

    moduleLengths <- M.fromList . map ((\[a,b] -> (a, read @Float b)) . splitOn ",") . drop 1 . lines
                <$> readFile "./rsc/modulenetlengths.csv"
    carrierLenghts <- M.fromList . map ((\[a,b] -> (a, read @Float b)) . splitOn ",") . drop 1 . lines
                <$> readFile "./rsc/carriernetlengths.csv"

    let mods     = [ (pinNumber1, netName1, component2, pinNumber2, netName2)
                   | (pinNumber1,netName1) <- fpgaNets
                   , (component2,pinNumber2,netName2) <- moduleNets
                   , netName1 == netName2 ] 

        pinMap'  = [ [ pinNumber1, netName1
                     , component2, show pinNumber2, netName2
                     , netName3 , component3, show pinNumber3 
                     ] ++ (map show $ wireStuff (M.findWithDefault 0.0 netName2 moduleLengths)
                                                (M.findWithDefault 0.0 netName3 carrierLenghts))
                   | (pinNumber1, netName1, component2, pinNumber2, netName2) <- mods
                   , (netName3,component3,pinNumber3) <- carrierNets
                   , (convert component2 pinNumber2) == (component3,pinNumber3) ]

        delays   = map (read @Float . (!!9)) pinMap'
        maxDelay = maximum delays
        taps     = map (show @Int . round . (/39.0e-12) . (maxDelay-) ) delays
        pinMap   = zipWith (\p t -> p ++ [t]) pinMap' taps

        csv      = header : pinMap

    writeFile "./rsc/pinmap.csv" <$> unlines $ map (intercalate ",") csv

    let (l1,l2,l3) = unzip3 $ map genVerilog pinMap
        verilog    = unlines l1 ++ "\n\n" ++ unlines l2 ++ "\n\n" ++ unlines l3
        
    writeFile "./rsc/code.v" verilog
    pure ()
  where
    header = [ "fpgaPin", "fpgaNet", "moduleComponent", "modulePin", "moduleNet"
             , "carrierNet", "carrierComponent", "carrierPin", "length", "delay"
             , "taps"]
